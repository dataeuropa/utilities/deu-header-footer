## [4.18.4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.18.3...v4.18.4) (2022-10-13)


### Bug Fixes

* Dynamically compute visibility of search bar ([b3e9e7d](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b3e9e7d708f07255b88f0354b167b9a9c65d4d3d))

## [4.18.3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.18.2...v4.18.3) (2022-10-13)


### Bug Fixes

* Adjust key for Search Bar ([0f4056d](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/0f4056d108461e40d73b336071bb5ae9cee97099))

## [4.18.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.18.1...v4.18.2) (2022-10-13)


### Bug Fixes

* Show/Hide Search Bar dynamically on Navigation ([193cc73](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/193cc73fa647e9a7e20ccd8523a4f0a8dadb2aaa))

## [4.18.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.18.0...v4.18.1) (2022-10-13)


### Bug Fixes

* Hide Search Bar only on Datasets and Catalogues main pages ([a8abc78](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a8abc789bfbf4369f598fffe864988e0e75a6b10))

# [4.18.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.17.0...v4.18.0) (2022-10-13)


### Features

* Hide search bar on Datasets and Catalogues page ([77e1041](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/77e10418159e964c616dd1948a1fe2d9d47ee159))

# [4.17.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.16.0...v4.17.0) (2022-10-13)


### Bug Fixes

* Translated using Weblate (English) ([d6751d6](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/d6751d66f8b2f4e5e005eaae476d3e3c7b8e6df0))
* Translated using Weblate (English) ([db72aea](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/db72aea14965ecb4a7ef7afde9c72224b451c48c))
* Translated using Weblate (English) ([91b3fa8](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/91b3fa8d3d56d26f4b09eff52f25b59b59ffdb63))
* Translated using Weblate (English) ([d589026](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/d589026eea1077bf947dd5de65825f032f00eaf6))
* Translated using Weblate (English) ([a2c0db0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a2c0db0914ad2f44d3079d7b536d517d01f47f73))


### Features

* Adjust header menu links and arrow rotation ([4ab7af2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/4ab7af21d4ddff1f6f700ea95056f0c9082d17fc))

# [4.16.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.7...v4.16.0) (2022-09-09)


### Features

* move menu items to HeaderBottom.vue ([5bfe10b](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/5bfe10b5e00b4f87c534b4689cc0943fb8fc5097))

## [4.15.7](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.6...v4.15.7) (2022-09-09)


### Bug Fixes

* correct languange selector styles ([05085e0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/05085e026590bb2d7e2ef85cb50b55102cac25d2))

## [4.15.6](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.5...v4.15.6) (2022-09-09)


### Bug Fixes

* class correction in HeaderTop.vue ([b24cc89](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b24cc89183d4f3d1634883c74cc39a61af5cc4fc))

## [4.15.5](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.4...v4.15.5) (2022-09-09)


### Bug Fixes

* adapt fonts ([d3224de](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/d3224de556ba5f5f4071e77316d7b71c02b5b086))

## [4.15.4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.3...v4.15.4) (2022-09-09)


### Bug Fixes

* adapt footer stylings ([8579795](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/857979538595229e588ad7f473aa401db8f81fb6))
* adapt header nav styles ([3d60bba](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3d60bba72b31ed8be6b12cd2b79f234754a9864e))
* adapt header top styles ([c730c60](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/c730c60d76b6bbc85c43cad285173c6aa15be85b))
* adapt logo size ([a4eb744](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a4eb74414a43d35e962cc5e53adba8a3b8c0c454))
* adapted nav items styles ([b6f6479](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b6f64792227f99f863b27763dd20f5179bfb03d1))
* language selector styles ([b1e9a2e](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b1e9a2ee42595867487d4ec7263b22704b816c2a))

## [4.15.3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.2...v4.15.3) (2022-08-26)


### Bug Fixes

* remove link from deu headline ([3c098ac](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3c098ac32c59bddef09a07224f292a51ee981fa3))

## [4.15.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.1...v4.15.2) (2022-08-26)


### Bug Fixes

* fix laptop view navigation display value ([3479f33](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3479f3352944cfb3f8673a261afd3ee81de37e35))

## [4.15.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.15.0...v4.15.1) (2022-08-26)


### Bug Fixes

* mobile navigation item positioning; mobile navigation translation key; transition bug while resizing ([4b8342a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/4b8342a5ecce1e5206963b472d30a9c593fd1cea))

# [4.15.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.14.0...v4.15.0) (2022-08-25)


### Bug Fixes

* Translated using Weblate (Bulgarian) ([b21e1ea](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b21e1eaa612ec9333187ea14f83a6d9143015be4))
* Translated using Weblate (English) ([df7d7a9](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/df7d7a9080f6b8e98f4463abbbf8f7294af18ecc))
* Translated using Weblate (English) ([3fcb80a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3fcb80a67898fb6b5d09b685ca514c5c962093ef))
* Translated using Weblate (English) ([023ec82](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/023ec8255c388b86efc96df625de3f3ea08ac15e))
* Translated using Weblate (English) ([3c26b78](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3c26b7803b26c1b042e59f987af52d0fa9555c69))
* Translated using Weblate (English) ([dd5c385](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/dd5c385692a2e63cb346e10c44145f42003bdc6a))
* Translated using Weblate (English) ([2b8345f](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/2b8345f48e133b9c04852a80aa9e6116692ff8e6))
* Translated using Weblate (English) ([ce9a670](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/ce9a670309ec4feb216ab7ee9e9007d1d09686c5))
* Translated using Weblate (English) ([a2ee668](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a2ee66859d0a6f0af9caa5307cb6909292744ef4))
* Translated using Weblate (English) ([26619a3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/26619a34fe5541e3fdeebe0b0e986cae585b82c9))
* Translated using Weblate (English) ([b23a63a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b23a63ab8e4fea7f97c0ca203ae663d6a7c71235))
* Translated using Weblate (English) ([5632cd9](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/5632cd945b3fb6f20849d2d32ee115ac086c987a))
* Translated using Weblate (English) ([e6a7960](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/e6a79601451b385cd73532f65abb308fed57510b))
* Translated using Weblate (English) ([5c3b541](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/5c3b54112d279dadcfa6889e2c0b42c8ead2faad))
* Translated using Weblate (English) ([3870252](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3870252f9643405ef5b5d67d0d682e5684c21101))
* Translated using Weblate (English) ([2beb58c](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/2beb58c85bbdddf4cafc50acfd661d133d09f823))
* Translated using Weblate (English) ([bf39e72](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/bf39e7252a37a8b7590176d5672223a4b242f9e6))
* Translated using Weblate (English) ([f2d4f35](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/f2d4f35f04f8b029aeefde0296bcccb357849847))
* Translated using Weblate (Italian) ([66e7cbf](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/66e7cbfc92b7f3a06dd41365b49308402c0ed2d4))
* Translated using Weblate (Spanish) ([0e8e16d](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/0e8e16d7553c7eb48bdc44703f2a8d7381cfcde2))


### Features

* new header footer styling ([6dcf3cd](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6dcf3cd8ec7a23b3134d90c52885f8fda96f4179))

# [4.14.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.13.1...v4.14.0) (2022-08-24)


### Features

* Add Licensing Assistant to menu ([add8605](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/add8605270c0bf09a96c9c8904da060787073f7d))

## [4.13.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.13.0...v4.13.1) (2022-08-15)


### Bug Fixes

* About Us footer link ([1c9a007](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/1c9a00731b1828f33c355702d49fa74249c2bba5))

# [4.13.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.12.5...v4.13.0) (2022-05-24)


### Features

* remove covid-19 sub menu from studies section ([49c3da0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/49c3da014ee3b23adc072f932242e460c0dd4549))

## [4.12.5](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.12.4...v4.12.5) (2022-05-18)


### Bug Fixes

* Translated using Weblate (English) ([6711cdf](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6711cdf412903f396deebc33772bfcff5371a894))

## [4.12.4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.12.3...v4.12.4) (2022-05-18)


### Bug Fixes

* Translated using Weblate (German) ([ae43a81](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/ae43a81e8069d72d8dd58d6efb8118a216ebede0))

## [4.12.3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.12.2...v4.12.3) (2022-05-03)


### Bug Fixes

* fix some vulnerabilities ([98cb495](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/98cb495a54c4fad6bddfc519378365b8a4ee1b4a))
* fix some vulnerabilities ([d6f6190](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/d6f619064f3dd30807656b12ed2782c4d63d28cc))

## [4.12.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.12.1...v4.12.2) (2022-04-21)


### Bug Fixes

* remove datathon banner from footer ([1a9cfce](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/1a9cfce80654cb6f8afb1c6685dcf510d224ebb9))

## [4.12.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.12.0...v4.12.1) (2022-03-01)


### Bug Fixes

* remove banners white border on for mqa page ([6f69c26](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6f69c26e4514a4c904bdb13e924df9fa22ba26aa))

# [4.12.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.11.3...v4.12.0) (2022-02-17)


### Features

* add image to footer ([0a4e0e2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/0a4e0e2f4c0b9128e1d9785785b5ebe38b510a93))
* change data stories to lowercase ([45d3300](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/45d3300fd640eacaf753749922fb5cd9e9efed44))


### Reverts

* Revert "add-image-to-footer" ([12341db](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/12341dbc78e2ee6986334ca8a99ae5a1a1bfb233))
* Revert "change Data stories to lowerCase" ([6dc863b](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6dc863b4d81e0160dfc36c7fe3e88dd26fa51a33))

## [4.11.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.11.1...v4.11.2) (2022-01-31)


### Bug Fixes

* fix search bar search path ([ce1519b](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/ce1519b84581e973bea3ff040797857c71ded0d5))

## [4.11.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.11.0...v4.11.1) (2022-01-31)


### Bug Fixes

* change search input placeholder ([98f22bc](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/98f22bc47cd8ba33de35efab605d71e12a9bd3d7))

# [4.11.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.10.0...v4.11.0) (2022-01-24)


### Features

* add data stories button below news ([bcfae57](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/bcfae57bbb0d14ca5dd8e5e86eed9249c6b2ad12))

# [4.10.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.9.3...v4.10.0) (2021-12-22)


### Features

* add facebook social button and change linkedin link ([682e239](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/682e239c8edc99de0d47c56c2be97583ac38ad25))

## [4.9.3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.9.2...v4.9.3) (2021-09-27)


### Bug Fixes

* remove all occurences of 'european data portal' ([7a3d19c](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/7a3d19c5892dfd2416244ace7103a40e9564908a))

## [4.9.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.9.1...v4.9.2) (2021-09-24)


### Bug Fixes

* remove all occurences of 'edp', 'edp3' or 'european' ([b9dbb63](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/b9dbb6355806e3cf5006eb5f0164534fef9f0808))

## [4.9.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.9.0...v4.9.1) (2021-09-23)


### Bug Fixes

* change academy link in header ([dfe1565](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/dfe1565bed2b02d2669eed941bd09fd8de2e77db))

# [4.9.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.8.2...v4.9.0) (2021-09-22)


### Features

* change training to data europa academy (in header) ([7d6b7c5](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/7d6b7c54a9b5b61d73b78584d3c82b0034956ab3))

## [4.8.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.8.1...v4.8.2) (2021-09-08)


### Bug Fixes

* **header:** point to correct sparql search path ([a93f94f](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a93f94fffb4fe24dd06b2871db03bf2847a0f01e))
* incorrect sparql search url ([a020dec](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a020dec939d094419238fb1ee01f2ba50a599d20))

## [4.8.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.8.0...v4.8.1) (2021-09-03)


### Bug Fixes

* set show sparql button property to true ([f1bf205](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/f1bf205a78507fdb3b41f2c72d7a677bfea0ad8a))

# [4.8.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.7.1...v4.8.0) (2021-08-19)


### Features

* add newsletter button ([975a4da](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/975a4da8e2760e751c45f0956c4740c06cff592f))

## [4.7.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.7.0...v4.7.1) (2021-08-06)


### Bug Fixes

* fix sparql not active ([3d11309](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/3d1130900566e1859a5a196d46d7233595c25541))

# [4.7.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.6.0...v4.7.0) (2021-08-06)


### Features

* remove contact from footer ([281cfd4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/281cfd446147029b6f10c7f5066b34f67c2a65ae))

# [4.6.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.6...v4.6.0) (2021-08-05)


### Features

* remove former data portal disclaimer ([e23a022](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/e23a0229787a4b3d3d68cd0582a6f021ec6e8cfd))

## [4.5.6](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.5...v4.5.6) (2021-07-28)


### Bug Fixes

* rollback data.europa academy to training, fix disable every second sub menu item ([d6abd1d](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/d6abd1de121725efa0b6ae09a0258433b842366a))

## [4.5.5](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.4...v4.5.5) (2021-07-28)


### Bug Fixes

* disable sparql by default ([e61f311](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/e61f3111ee37f41f48f252cca83dc42fb506b215))

## [4.5.4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.3...v4.5.4) (2021-07-23)


### Bug Fixes

* fix sparql not disabled by default ([db29df7](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/db29df73a8a1ad53679c228113f49e5e64999fe2))

## [4.5.3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.2...v4.5.3) (2021-07-23)


### Bug Fixes

* fix sparql is not disabled by default ([c9d2d81](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/c9d2d81963d9d9de520713c26879da1acd48c51b))

## [4.5.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.1...v4.5.2) (2021-07-22)


### Bug Fixes

* fix curriculum link ([01ef7a8](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/01ef7a862fc79dbb72eac8d85b2444b20a10e252))

## [4.5.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.5.0...v4.5.1) (2021-07-22)


### Bug Fixes

* fix former datasets link ([dd36fa8](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/dd36fa8ac9c0851524c932b47ecb468a274788fd))

# [4.5.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.4.2...v4.5.0) (2021-07-21)


### Features

* add new header sub nav item (curriculum), change german translation for data provider's area ([cdc2f1e](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/cdc2f1e056975a0ce4dae55f2b1be81e4ed58e8a))

## [4.4.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.4.1...v4.4.2) (2021-07-21)


### Bug Fixes

* fix former datasets positioning ([7e3f91c](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/7e3f91caecc308cdcd51f7e178c5297053b73405))

## [4.4.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.4.0...v4.4.1) (2021-07-21)


### Bug Fixes

* fix former datasets link ([c3910c1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/c3910c186a8ff4514911a4b38cc6145e2b6b731a))

# [4.4.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.3.1...v4.4.0) (2021-07-21)


### Features

* change header navigation translations, new translation for former datasets, put about in footer, remove site branding text, add translation item for data provider's area ([a560cee](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a560cee7992b54ef361a97e9d8ff5ef965632a9e))

## [4.3.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.3.0...v4.3.1) (2021-07-16)


### Bug Fixes

* change search button hover color ([7641ff4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/7641ff4e307525e27e1486afe6c5fc40a04555f1))

# [4.3.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.2.1...v4.3.0) (2021-07-14)


### Features

* show/hide sparql search link via config ([e0ffa00](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/e0ffa001750a512883a293c5bde787a4c00a5b29))

## [4.2.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.2.0...v4.2.1) (2021-07-07)


### Bug Fixes

* change h2 to div in Header ([6d26b6f](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6d26b6fa745b5d649a2487525f4b6e2f376a662b))

## [4.2.1-beta.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.2.0...v4.2.1-beta.1) (2021-07-07)


### Bug Fixes

* change h2 to div in Header ([6d26b6f](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6d26b6fa745b5d649a2487525f4b6e2f376a662b))

# [4.2.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.1.0...v4.2.0) (2021-06-18)


### Features

* change subheading text again ([64e0bc2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/64e0bc25ce1c810ba10bea7de869887b6efbecf5))

# [4.2.0-beta.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.1.0...v4.2.0-beta.1) (2021-06-18)


### Features

* change subheading text again ([64e0bc2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/64e0bc25ce1c810ba10bea7de869887b6efbecf5))

# [4.1.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.2...v4.1.0) (2021-06-16)


### Features

* **footer:** add data provider's area link to footer ([f09e6cf](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/f09e6cffd053f5e11b36b32b51f062d4fe394e86))

## [4.0.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.1...v4.0.2) (2021-06-16)


### Bug Fixes

* **header:** set contact url to relative path ([943d815](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/943d815639ede800f670a267e90878df9f796bb5))

## [4.0.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0...v4.0.1) (2021-06-15)


### Bug Fixes

* **header:** prioritise anchor coloring ([43276a4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/43276a48c12689ec1ce64b0c7a9369284946611f))

## [4.0.1-beta.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0...v4.0.1-beta.1) (2021-06-15)


### Bug Fixes

* **header:** prioritise anchor coloring ([43276a4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/43276a48c12689ec1ce64b0c7a9369284946611f))

# [4.0.0-beta.8](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.7...v4.0.0-beta.8) (2021-06-15)

### Bug Fixes

* **header:** prioritise anchor coloring ([43276a4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/43276a48c12689ec1ce64b0c7a9369284946611f))
# [4.0.0](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v3.4.0...v4.0.0) (2021-06-15)


### Bug Fixes

* improve header subtitle css priorisation ([a476351](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a476351b72af6366048cd17c156d395f03582a48))


### Features

* add 'target=_blank' for 'Persisten URIs' and 'EU Vocabularies' tabs ([7185d6a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/7185d6a8083f240b230a5ce019d92b062ff75574))
* add 'target=_blank' for 'Persisten URIs' and 'EU Vocabularies' tabs ([31d7177](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/31d71774930c505f3b66203f2fbdd41b96a701a3))
* add configuration options for header and footer component names ([ee263a7](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/ee263a76722f658f5f6dc97a888b4b31040364b4))
* add contact link to menu ([c653c59](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/c653c59fc54b4b1847fb49d5734c272d796b64f7))
* add underline on hover above branding container ([4daa908](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/4daa90805e238e2977667e3946ddcf71f6d9c048))
* change sub heading text ([e97060a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/e97060a8e47bde6a9eeed71d4c1169eb9d0332b4))
* change sub heading text ([2ef6184](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/2ef6184eae5438a99daf073a801194e5948e8551))
* improve mobile visibility of language selector and search bar ([899d250](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/899d2500c427502d58939a305b8efbf6da6dc879))


### BREAKING CHANGES

* default component names are now 'deu-header' and 'deu-footer'

# [4.0.0-beta.7](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.6...v4.0.0-beta.7) (2021-06-15)


### Features

* add underline on hover above branding container ([4daa908](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/4daa90805e238e2977667e3946ddcf71f6d9c048))

# [4.0.0-beta.6](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.5...v4.0.0-beta.6) (2021-06-15)


### Features

* improve mobile visibility of language selector and search bar ([899d250](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/899d2500c427502d58939a305b8efbf6da6dc879))

# [4.0.0-beta.5](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.4...v4.0.0-beta.5) (2021-06-15)


### Features

* add contact link to menu ([c653c59](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/c653c59fc54b4b1847fb49d5734c272d796b64f7))

# [4.0.0-beta.4](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.3...v4.0.0-beta.4) (2021-06-15)


### Features

* add 'target=_blank' for 'Persisten URIs' and 'EU Vocabularies' tabs ([7185d6a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/7185d6a8083f240b230a5ce019d92b062ff75574))

# [3.4.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.3.0...v3.4.0) (2021-05-27)
# [4.0.0-beta.3](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.2...v4.0.0-beta.3) (2021-06-15)


### Features

* apply all neccessary optimization improvements ([6464cc1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/6464cc1af6e4e1e7aa8cc0270a2609866bb1f4d7))
* change sub heading text ([e97060a](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/e97060a8e47bde6a9eeed71d4c1169eb9d0332b4))

# [4.0.0-beta.2](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v4.0.0-beta.1...v4.0.0-beta.2) (2021-06-15)


### Bug Fixes

* improve header subtitle css priorisation ([a476351](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/a476351b72af6366048cd17c156d395f03582a48))

# [4.0.0-beta.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v3.3.0...v4.0.0-beta.1) (2021-06-15)


### Features

* add 'target=_blank' for 'Persisten URIs' and 'EU Vocabularies' tabs ([31d7177](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/31d71774930c505f3b66203f2fbdd41b96a701a3))
* add configuration options for header and footer component names ([ee263a7](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/ee263a76722f658f5f6dc97a888b4b31040364b4))
* apply all neccessary optimization improvements ([6464cc1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6464cc1af6e4e1e7aa8cc0270a2609866bb1f4d7))


### BREAKING CHANGES

* default component names are now 'deu-header' and 'deu-footer'

# [3.4.0-beta.1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/compare/v3.3.0...v3.4.0-beta.1) (2021-06-15)


### Features

* add 'target=_blank' for 'Persisten URIs' and 'EU Vocabularies' tabs ([31d7177](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/31d71774930c505f3b66203f2fbdd41b96a701a3))
* apply all neccessary optimization improvements ([6464cc1](https://gitlab.fokus.fraunhofer.de/data.europa.eu/utilities/deu-header-footer/commit/6464cc1af6e4e1e7aa8cc0270a2609866bb1f4d7))

# [3.4.0-beta.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.4.0-beta.1...v3.4.0-beta.2) (2021-06-09)


### Features

* add 'target=_blank' for 'Persisten URIs' and 'EU Vocabularies' tabs ([31d7177](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/31d71774930c505f3b66203f2fbdd41b96a701a3))

# [3.4.0-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.3.0...v3.4.0-beta.1) (2021-05-27)


### Features

* apply all neccessary optimization improvements ([6464cc1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/6464cc1af6e4e1e7aa8cc0270a2609866bb1f4d7))

# [3.3.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.2.0...v3.3.0) (2021-05-07)


### Features

* update translations ([a319bc3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/a319bc35e376115f1e6f0a85f4ec09a9de55cba7))

# [3.2.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.8...v3.2.0) (2021-05-07)


### Features

* update translations ([2beabfc](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/2beabfc8ad209f650b49e6ebaab8113f3e979a01))

## [3.1.8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.7...v3.1.8) (2021-05-06)


### Bug Fixes

* fix footer linkedin link; fix searchbar translations ([716667b](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/716667b3a1159d78aefa724cf70ea0b0381df62d))

## [3.1.7](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.6...v3.1.7) (2021-05-06)


### Bug Fixes

* change tagline for it ([3fe9a2a](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/3fe9a2a3fe121cca94122cb2773be310d2e91692))

## [3.1.6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.5...v3.1.6) (2021-05-05)


### Bug Fixes

* change to selected language instead of en after click on link here ([7aa01a6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/7aa01a6f84f2d3f190699709796be49620442c3e))

## [3.1.5](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.4...v3.1.5) (2021-04-29)


### Bug Fixes

* keep language after page refresh (statistics) ([932604c](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/932604c9678af64a612f57a1b7954a5609aeb372))

## [3.1.4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.3...v3.1.4) (2021-04-28)


### Bug Fixes

* change language after navigating back via browser back button to another language ([be2011c](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/be2011c1ed062c9195d99fca4ba10e110d0813eb))

## [3.1.3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.2...v3.1.3) (2021-04-22)


### Bug Fixes

* **Header:** translatable subtitle ([9a4856c](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9a4856ce8788bc216d8db0c546b5660fb9ca343e))

## [3.1.3-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.2...v3.1.3-beta.1) (2021-04-21)


### Bug Fixes

* **Header:** translatable subtitle ([9a4856c](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9a4856ce8788bc216d8db0c546b5660fb9ca343e))

## [3.1.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.1...v3.1.2) (2021-04-21)


### Bug Fixes

* **Header:** 'here' link now points to the correct one ([c00d50a](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c00d50a44e9e9ada64d86efbee773f92fb5615eb))

## [3.1.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.0...v3.1.1) (2021-04-21)


### Bug Fixes

* **Header:** add underline style for 'here' link ([3307b26](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/3307b261f4a7c7de85d98ad2efaf1b65f3b548f4))
* **Header:** correct href of 'here' link ([5d7035f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5d7035f75761b528edd46f3e37deb3f126ef483e))

## [3.1.1-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.0...v3.1.1-beta.1) (2021-04-21)


### Bug Fixes

* **Header:** add underline style for 'here' link ([3307b26](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/3307b261f4a7c7de85d98ad2efaf1b65f3b548f4))
* **Header:** correct href of 'here' link ([5d7035f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5d7035f75761b528edd46f3e37deb3f126ef483e))

# [3.1.0-beta.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.1.0-beta.1...v3.1.0-beta.2) (2021-04-21)

### Bug Fixes

* **Header:** add underline style for 'here' link ([3307b26](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/3307b261f4a7c7de85d98ad2efaf1b65f3b548f4))
* **Header:** correct href of 'here' link ([5d7035f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5d7035f75761b528edd46f3e37deb3f126ef483e))
# [3.1.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0...v3.1.0) (2021-04-19)


### Bug Fixes

* **Header:** make search button round for mobile ([9d40f9c](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9d40f9ca7e83c844361cffb8c2b042a6c59d34bb))
* **Header:** use vertical logo for mobile ([ae44a99](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/ae44a9929f69aaf18c5cf26f6ec7d1cb353e9421))


### Features

* **Footer:** remove 'help us improve' box ([f0d49b6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/f0d49b6fc9ea38b37321558c43ae76cb4c9cf9fb))
* **Footer:** remove menu item 'newsletter' ([e6216e4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e6216e422fd6221b630ddf016731a61296c332b3))
* **Header:** add banner 'discover' ([7809c4d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/7809c4d246adfbb4e6de08a9f67195184e8eba2f))
* **Header:** adjust sparql manager url to '/sparql/' ([ff05bba](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/ff05bba7e8a0dbf33b80212552c86ad370075dcb))

# [3.1.0-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.1-beta.1...v3.1.0-beta.1) (2021-04-19)


### Features

* **Footer:** remove 'help us improve' box ([f0d49b6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/f0d49b6fc9ea38b37321558c43ae76cb4c9cf9fb))
* **Footer:** remove menu item 'newsletter' ([e6216e4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e6216e422fd6221b630ddf016731a61296c332b3))
* **Header:** add banner 'discover' ([7809c4d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/7809c4d246adfbb4e6de08a9f67195184e8eba2f))
* **Header:** adjust sparql manager url to '/sparql/' ([ff05bba](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/ff05bba7e8a0dbf33b80212552c86ad370075dcb))

## [3.0.1-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0...v3.0.1-beta.1) (2021-04-19)


### Bug Fixes

* **Header:** make search button round for mobile ([9d40f9c](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9d40f9ca7e83c844361cffb8c2b042a6c59d34bb))
* **Header:** use vertical logo for mobile ([ae44a99](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/ae44a9929f69aaf18c5cf26f6ec7d1cb353e9421))

# [3.0.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.3.0...v3.0.0) (2021-04-14)


### Bug Fixes

* **Header:** match breakpoint until mobile header with data.europa.eu ([5ae4895](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5ae4895f981deb572cca8df52cfa98e884819f3a))


### Features

* **Footer:** add commission link ([f3aa9cb](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/f3aa9cb95874c5892c6b632fcba07180e316649f))
* **Footer:** migrate to data.europa.eu design ([132b825](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/132b825071e2c8c541575af3adbae7ec3149f2b2))
* **Footer:** remove cookie link ([d3de151](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d3de1510510a891058a28d269c1890def810475c))
* **Footer:** remove facebook social media ([d4133ad](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d4133ad70609ebd2be4e68308c17279cfabdf19b))
* **Footer:** remove privacy statement menu item ([0aa3475](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/0aa347571792de2617b7e269cf62c0bdf43f635a))
* **Footer:** remove slogan ([1c99776](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/1c99776da6b4d9be6ff78adfe13ac99ebfd86db9))
* **header:** remove teaser banner ([671a3ca](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/671a3ca35849115d05042f64f8af056ead92865f))
* **Header:** add submenu items 'persistent uri' and 'eu vocabularies' ([5d2a16a](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5d2a16a6ad5d5735581b96bcd3c1d6c5cdc0bad6))
* **Header:** adjust 'about' submenu items ([c00970d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c00970dae3a58acf9b47e3142f3c7ec8649c546a))
* **Header:** adjust social media links ([8082c85](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/8082c858dd89c6ee7874dc55a5281aca154129df))
* **Header:** migrate to data.europa.eu design ([2749444](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/274944478b91a83402bf3de5b494cf364d99479c))
* **Header:** reduce navbar font sizing ([e184efc](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e184efc51afaf679b63ebe7349776a1775406d11))
* **Header:** remove from training all submenu items except e-learning and licensing assistant ([cd0211f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/cd0211fc3e8a3ee9f266107f0292044dcc3d79ce))
* **Header:** remove locale no from language selector ([037ff82](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/037ff8268fae34645561027466016a201c2927f1))
* **Header:** remove submenu item 'open data impact' ([59f0442](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/59f044257f4d78f5d9f698771f855115a44e8bf6))


### BREAKING CHANGES

* **Footer:** The footer has been adjusted to data.europa.eu design
* **Header:** The design has been adjusted to data.europa.eu design

# [3.0.0-beta.8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.7...v3.0.0-beta.8) (2021-04-14)


### Features

* **Footer:** remove cookie link ([d3de151](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d3de1510510a891058a28d269c1890def810475c))
* **Footer:** remove slogan ([1c99776](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/1c99776da6b4d9be6ff78adfe13ac99ebfd86db9))
* **Header:** adjust 'about' submenu items ([c00970d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c00970dae3a58acf9b47e3142f3c7ec8649c546a))
* **Header:** reduce navbar font sizing ([e184efc](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e184efc51afaf679b63ebe7349776a1775406d11))

# [3.0.0-beta.7](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.6...v3.0.0-beta.7) (2021-04-13)


### Features

* **Footer:** add commission link ([f3aa9cb](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/f3aa9cb95874c5892c6b632fcba07180e316649f))

# [3.0.0-beta.6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.5...v3.0.0-beta.6) (2021-04-13)


### Features

* **Footer:** remove privacy statement menu item ([0aa3475](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/0aa347571792de2617b7e269cf62c0bdf43f635a))

# [3.0.0-beta.5](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.4...v3.0.0-beta.5) (2021-04-13)


### Features

* **Footer:** remove facebook social media ([d4133ad](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d4133ad70609ebd2be4e68308c17279cfabdf19b))
* **Header:** adjust social media links ([8082c85](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/8082c858dd89c6ee7874dc55a5281aca154129df))

# [3.0.0-beta.4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.3...v3.0.0-beta.4) (2021-04-13)


### Features

* **Header:** remove locale no from language selector ([037ff82](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/037ff8268fae34645561027466016a201c2927f1))

# [3.0.0-beta.3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.2...v3.0.0-beta.3) (2021-04-13)


### Bug Fixes

* **Header:** match breakpoint until mobile header with data.europa.eu ([5ae4895](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5ae4895f981deb572cca8df52cfa98e884819f3a))

# [3.0.0-beta.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v3.0.0-beta.1...v3.0.0-beta.2) (2021-04-13)


### Features

* **Header:** add submenu items 'persistent uri' and 'eu vocabularies' ([5d2a16a](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5d2a16a6ad5d5735581b96bcd3c1d6c5cdc0bad6))
* **Header:** remove from training all submenu items except e-learning and licensing assistant ([cd0211f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/cd0211fc3e8a3ee9f266107f0292044dcc3d79ce))
* **Header:** remove submenu item 'open data impact' ([59f0442](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/59f044257f4d78f5d9f698771f855115a44e8bf6))

# [3.0.0-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.3.0...v3.0.0-beta.1) (2021-04-08)


### Features

* **Footer:** migrate to data.europa.eu design ([132b825](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/132b825071e2c8c541575af3adbae7ec3149f2b2))
* **header:** remove teaser banner ([671a3ca](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/671a3ca35849115d05042f64f8af056ead92865f))
* **Header:** migrate to data.europa.eu design ([2749444](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/274944478b91a83402bf3de5b494cf364d99479c))


### BREAKING CHANGES

* **Footer:** The footer has been adjusted to data.europa.eu design
* **Header:** The design has been adjusted to data.europa.eu design

# [2.3.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0...v2.3.0) (2021-03-29)


### Features

* add i18n translation ([eac9e81](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/eac9e810fb4f2591f03942a6a38c8837db79c6a9))
* add teaser banner ([65a0f1f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/65a0f1f9075de9dbf330ea27e31ef56e0771cf62))


### Reverts

* Revert "fix: revertme" ([35f2c96](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/35f2c966e8a7edd179dfc6d6541220cf62ac9825))
* Revert "feat(__test): bump version" ([bde371d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/bde371d8c3377f7897f2ffc6cdc640b25a45b8da))
* Revert "feat(__test): bump" ([06dd712](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/06dd71214e420dc12a8a159495e91ea04c453ff4))
* Revert "feat(__test): testing ci deployment trigger" ([c48bd0f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c48bd0f7694681425e94e142bdd85ec35febb14c))
* Revert "fix(__test): bump" ([b4cdccb](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/b4cdccbb43f5213f01db0905bb3703a61297b93a))
* Revert "fix(__test): yet another bump" ([d9d7c98](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d9d7c984ff6988530c123685333b6a8160d87019))
* Revert "fix(__test): one more bump" ([e6979d8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e6979d80aa69846d2672cb9da59925867ccbc92f))
* Revert "fix(__test): one more bump" ([52e2440](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/52e2440eb02cd3b98d6c18cfaaddd0d90a073e22))

# [2.3.0-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0...v2.3.0-beta.1) (2021-03-29)


### Features

* add i18n translation ([eac9e81](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/eac9e810fb4f2591f03942a6a38c8837db79c6a9))
* add teaser banner ([65a0f1f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/65a0f1f9075de9dbf330ea27e31ef56e0771cf62))


### Reverts

* Revert "fix: revertme" ([35f2c96](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/35f2c966e8a7edd179dfc6d6541220cf62ac9825))
* Revert "feat(__test): bump version" ([bde371d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/bde371d8c3377f7897f2ffc6cdc640b25a45b8da))
* Revert "feat(__test): bump" ([06dd712](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/06dd71214e420dc12a8a159495e91ea04c453ff4))
* Revert "feat(__test): testing ci deployment trigger" ([c48bd0f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c48bd0f7694681425e94e142bdd85ec35febb14c))
* Revert "fix(__test): bump" ([b4cdccb](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/b4cdccbb43f5213f01db0905bb3703a61297b93a))
* Revert "fix(__test): yet another bump" ([d9d7c98](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d9d7c984ff6988530c123685333b6a8160d87019))
* Revert "fix(__test): one more bump" ([e6979d8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e6979d80aa69846d2672cb9da59925867ccbc92f))
* Revert "fix(__test): one more bump" ([52e2440](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/52e2440eb02cd3b98d6c18cfaaddd0d90a073e22))

# [2.2.0-beta.19](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.18...v2.2.0-beta.19) (2021-03-26)


### Features

* add i18n translation ([eac9e81](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/eac9e810fb4f2591f03942a6a38c8837db79c6a9))

# [2.2.0-beta.18](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.17...v2.2.0-beta.18) (2021-03-26)


### Features

* add teaser banner ([65a0f1f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/65a0f1f9075de9dbf330ea27e31ef56e0771cf62))

# [2.2.0-beta.17](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.16...v2.2.0-beta.17) (2021-03-16)


### Reverts

* Revert "fix: revertme" ([35f2c96](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/35f2c966e8a7edd179dfc6d6541220cf62ac9825))
* Revert "fix: revertme" ([51a88f4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/51a88f4aa1b0a0b2aea9ee01620ce9f4a72d814c))

# [2.2.0-beta.16](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.15...v2.2.0-beta.16) (2021-03-16)


### Bug Fixes

* revertme ([5381cd8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5381cd82924c1ba7f180808cde8b5ca32ab7b10b))

# [2.2.0-beta.15](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.14...v2.2.0-beta.15) (2021-03-16)


### Bug Fixes

* revertme ([d69d772](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d69d7728fd569df0fb6fdf4831412eac45e65a42))

# [2.2.0-beta.14](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.13...v2.2.0-beta.14) (2021-01-20)


### Reverts

* Revert "feat(__test): bump version" ([bde371d](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/bde371d8c3377f7897f2ffc6cdc640b25a45b8da))

# [2.2.0-beta.13](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.12...v2.2.0-beta.13) (2021-01-20)


### Features

* **__test:** bump version ([c1a5006](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c1a50068a5bc35f47ef3cb200d9c99cc0b274a3b))

# [2.2.0-beta.12](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.11...v2.2.0-beta.12) (2021-01-20)


### Reverts

* Revert "feat(__test): bump" ([06dd712](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/06dd71214e420dc12a8a159495e91ea04c453ff4))

# [2.2.0-beta.11](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.10...v2.2.0-beta.11) (2021-01-20)


### Features

* **__test:** bump ([f66100f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/f66100f4683210e21aab6fe5bf540dc99099b4fc))

# [2.2.0-beta.10](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.9...v2.2.0-beta.10) (2021-01-20)


### Reverts

* Revert "feat(__test): testing ci deployment trigger" ([c48bd0f](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/c48bd0f7694681425e94e142bdd85ec35febb14c))
* Revert "fix(__test): bump" ([b4cdccb](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/b4cdccbb43f5213f01db0905bb3703a61297b93a))
* Revert "fix(__test): yet another bump" ([d9d7c98](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d9d7c984ff6988530c123685333b6a8160d87019))
* Revert "fix(__test): one more bump" ([e6979d8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/e6979d80aa69846d2672cb9da59925867ccbc92f))
* Revert "fix(__test): one more bump" ([52e2440](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/52e2440eb02cd3b98d6c18cfaaddd0d90a073e22))

# [2.2.0-beta.9](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.8...v2.2.0-beta.9) (2021-01-19)
# [2.2.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.1.4...v2.2.0) (2021-01-12)


### Bug Fixes

* **__test:** one more bump ([0ef89e8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/0ef89e8cee7fe1b50c1e6a9daf76c5e588ef0d8e))

# [2.2.0-beta.8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.7...v2.2.0-beta.8) (2021-01-19)


### Bug Fixes

* **__test:** one more bump ([a3d9899](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/a3d9899d06a49949499f65d87d4f0461165f977d))

# [2.2.0-beta.7](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.6...v2.2.0-beta.7) (2021-01-19)


### Bug Fixes

* **__test:** yet another bump ([5defde5](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5defde5f8a6041209ccf547ccdb9e2881a8b4ca8))

# [2.2.0-beta.6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.5...v2.2.0-beta.6) (2021-01-19)


### Bug Fixes

* **__test:** bump ([06aebae](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/06aebae55ff2c1d3533dfd55ad513cf203139c50))

# [2.2.0-beta.5](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.4...v2.2.0-beta.5) (2021-01-19)
* **search bar:** add description to aria label ([d965fa8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d965fa83b83e3f14d16c982bde681b30857db739))


### Features

* **__test:** testing ci deployment trigger ([5b074a3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/5b074a3c40a5d9b7b8f82be6a7340333f453b1e2))
* change footer headings to h4; text below help us improve looks better; change header brand name to h1; add aria-label and role attribute to language picker, search field and search field dropdown; change active and hover background-color of header subnavigation ([6c3599e](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/6c3599eb46c00aa992ec1854718571635fde4a80))
* remove some !important in header and footer, change social network link font size ([ca09544](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/ca09544a974540a353ed31b1b304d84844efcf5c))

# [2.2.0-beta.4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.3...v2.2.0-beta.4) (2021-01-12)


### Bug Fixes

* **header:** Fixed Wrong House Logo Link ([3037fb5](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/3037fb5d3949da9ef3e1ee9dee5db4148966af75))
* **header:** Fixed Wrong Main Logo Link ([9f6b911](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9f6b911578ca2d0c65d38124bc2f766a0687869f))

# [2.2.0-beta.3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.2...v2.2.0-beta.3) (2021-01-12)


### Bug Fixes

* **search bar:** add description to aria label ([d965fa8](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d965fa83b83e3f14d16c982bde681b30857db739))

# [2.2.0-beta.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.2.0-beta.1...v2.2.0-beta.2) (2021-01-06)


### Features

* remove some !important in header and footer, change social network link font size ([ca09544](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/ca09544a974540a353ed31b1b304d84844efcf5c))

# [2.2.0-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.1.2...v2.2.0-beta.1) (2020-12-16)


### Features

* change footer headings to h4; text below help us improve looks better; change header brand name to h1; add aria-label and role attribute to language picker, search field and search field dropdown; change active and hover background-color of header subnavigation ([6c3599e](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/6c3599eb46c00aa992ec1854718571635fde4a80))
## [2.1.4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.1.3...v2.1.4) (2020-12-23)


### Bug Fixes

* **header:** Fixed Wrong House Logo Link ([3037fb5](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/3037fb5d3949da9ef3e1ee9dee5db4148966af75))

## [2.1.3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.1.2...v2.1.3) (2020-12-23)


### Bug Fixes

* **header:** Fixed Wrong Main Logo Link ([9f6b911](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9f6b911578ca2d0c65d38124bc2f766a0687869f))

## [2.1.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.1.1...v2.1.2) (2020-12-16)


### Bug Fixes

* **header:** adjust all header links to be consistent between different router bases ([0594755](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/05947557d89d6de99a6c63604be88276f5158e3a))

## [2.1.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.1.0...v2.1.1) (2020-12-16)


### Bug Fixes

* **header:** fix header links when using different base path ([9dac10b](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/9dac10b27be1cc7cdb16d2dc06fcea9615f22a6d))

# [2.1.0](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.0.1...v2.1.0) (2020-12-09)


### Features

* **footer:** add privacy statement link ([0e5a9a4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/0e5a9a46d375456bf4e9107bd547bbfcd3bbbbb9))

# [2.1.0-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.0.1...v2.1.0-beta.1) (2020-12-09)


### Features

* **footer:** add privacy statement link ([0e5a9a4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/0e5a9a46d375456bf4e9107bd547bbfcd3bbbbb9))

## [2.0.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.0.0...v2.0.1) (2020-11-30)


### Bug Fixes

* **header:** prevent navbar collapsing on click ([09180d6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/09180d611b425a86ec1e8e11bf6d1c5f0cf32614))

## [2.0.1-beta.3](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.0.1-beta.2...v2.0.1-beta.3) (2020-11-30)


### Bug Fixes

* **header:** prevent navbar collapsing on click ([09180d6](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/09180d611b425a86ec1e8e11bf6d1c5f0cf32614))

## [2.0.1-beta.2](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.0.1-beta.1...v2.0.1-beta.2) (2020-11-30)


### Reverts

* "fix(header): prevent navbar collapsing on click" ([39844b4](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/39844b4468109f1e59ef47909b92727eeb4a2abe))

## [2.0.1-beta.1](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//compare/v2.0.0...v2.0.1-beta.1) (2020-11-30)


### Bug Fixes

* **header:** prevent navbar collapsing on click ([d94cadf](https://gitlab.fokus.fraunhofer.de/european-data-portal-2/hub/edp-header-footer//commit/d94cadf4bf3016356394661486dac89be16ace5e))
