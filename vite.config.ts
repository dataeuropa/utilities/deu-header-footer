import eslint from '@nabla/vite-plugin-eslint';
import vue from '@vitejs/plugin-vue';
import autoprefixer from 'autoprefixer';
import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';


import path from 'node:path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    dts({ outputDir: 'dist/types' }),
    eslint(),
  ],

  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
  },

  css: {
    postcss: {
      plugins: [
        autoprefixer({}),
      ]
    },
    preprocessorOptions: {
      scss: {
        // example : additionalData: `@import "./src/design/styles/variables";`
        // dont need include file extend .scss
        additionalData: '@import "@/styles/variables";',
      },
    },
  },

  server: {
    port: 8080,
  },

  build: {
    assetsInlineLimit: 45096,
    sourcemap: true,
    lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: path.resolve(__dirname, 'src/library-plugin.ts'),
      name: 'deu-header-footer',
      // the proper extensions will be added
      fileName: 'deu-header-footer',
      formats: ['es'],
    },
    rollupOptions: {
      strictDeprecations: true,
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: [
        'vue',
        '@ecl/preset-ec',
        '@fortawesome/fontawesome-svg-core',
        '@fortawesome/free-brands-svg-icons',
        '@fortawesome/free-regular-svg-icons',
        '@fortawesome/free-solid-svg-icons',
        '@fortawesome/vue-fontawesome',
      ],
      input: 'src/library-plugin.ts',
      // input: Object.fromEntries(
      //   glob.sync('{src/components/**/*.{vue,js,ts},src/library-plugin.ts}').map((file) => [
      //     // This remove `src/` as well as the file extension from each file, so e.g.
      //     // src/nested/foo.js becomes nested/foo
      //     path.relative('src/', file.slice(0, file.length - path.extname(file).length)),
      //     // This expands the relative paths to absolute paths, so e.g.
      //     // src/nested/foo becomes /project/src/nested/foo.js
      //     fileURLToPath(new URL(file, import.meta.url)),
      //   ]),
      // ),
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
        entryFileNames: (chunkInfo) => {
          // if chunkInfo.name starts with node_modules, replace with external
          if (chunkInfo.name.startsWith('node_modules')) {
            return chunkInfo.name.replace('node_modules', 'external') + '.mjs';
          }
          return '[name].mjs';
        },
        assetFileNames: 'deu-header-footer.[ext]',
        preserveModules: true,
        preserveModulesRoot: 'src',
      },
    },
  },
});
