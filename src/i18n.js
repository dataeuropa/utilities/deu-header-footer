import { createI18n } from 'vue-i18n';

// Vue-i18n setup
const i18nInstance = createI18n({
  locale: 'en',
  fallbackLocale: 'en',
  // messages: process.i18n,
  silentTranslationWarn: true,
  legacy: false,
});

export default i18nInstance;
