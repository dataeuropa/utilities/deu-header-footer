// eslint-disable no-unused-vars
import * as Router from 'vue-router';

import MainContent from './components/pages/MainContent.vue';
import HelloWorld from './components/pages/HelloWorld.vue';

const routes = [
  {
    name: 'Default',
    path: '/',
    component: MainContent,
    meta: {
      breadcrumbs: [
        { text: 'Main', to: { path: '/hello' } },
      ],
    },
  },
  {
    name: 'HelloWorld',
    path: '/hello',
    component: HelloWorld,
    meta: {
      breadcrumbs: [
        { text: 'Home', to: { path: '/' } },
        { text: 'Hello' },
      ],
    },
  },
];

const router = Router.createRouter({
  history: Router.createWebHistory('/'),
  routes,
});

export default router;
