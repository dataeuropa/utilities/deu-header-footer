import type { Plugin } from 'vue';
// Registering icons
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faHome, faChevronDown, faBars, faTimes, faSearch, faExternalLinkAlt, faCheck, faCaretRight,
} from '@fortawesome/free-solid-svg-icons';
import {
  faComments,
} from '@fortawesome/free-regular-svg-icons';
import {
  faTwitter, faYoutube,
  faTwitterSquare, faLinkedin, faYoutubeSquare,
} from '@fortawesome/free-brands-svg-icons';

import Header from './components/header/Header.vue';
import Footer from './components/footer/Footer.vue';

// Note: doesn't cause side effect when built.
import './styles/globals.scss';
import messages from '../i18n/i18n';
import type { I18n } from 'vue-i18n';


let isInstalled = false;

function registerIcons() {
  library.add(
    faHome,
    faChevronDown,
    faBars,
    faTimes,
    faSearch,
    faExternalLinkAlt,
    faComments,
    faCheck,
    faCaretRight,
  );

  library.add(
    faTwitter,
    faYoutube,
    faTwitterSquare,
    faLinkedin,
    faYoutubeSquare,
  );
}

export interface HeaderFooterPluginOptions {
  i18n: I18n;
}

/**
 * Vue plugin to register necessary icons to make the header and footer components work properly.
 */
const plugin : Plugin<HeaderFooterPluginOptions> = {
  install(_, options: HeaderFooterPluginOptions) {
    if (isInstalled) return;
    isInstalled = true;

    const { i18n } = options;

    Object.keys(messages).forEach((key) => {
      const msg = messages[key as keyof typeof messages] as Record<string, any>;
      i18n.global.mergeLocaleMessage(key, msg);
    });

    registerIcons();
  },
};

export default plugin;
// It's redundant, but some prefer it (e.g., IDE suggestions)
export { plugin };

export { Header };
export { Footer };
