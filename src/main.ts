import { createApp } from 'vue';
import App from './App.vue';

import i18n from './i18n';
import router from './router';
import deuFooterHeader, { Header, Footer } from './library-plugin';

import 'bootstrap';
import 'bootstrap/scss/bootstrap.scss';
import '@ecl/preset-ec/dist/styles/ecl-ec.css';

const app = createApp(App);

app.use(deuFooterHeader, { i18n });
app.component('deu-header', Header);
app.component('deu-footer', Footer);

app.use(i18n);
app.use(router);

app.mount('#app');
