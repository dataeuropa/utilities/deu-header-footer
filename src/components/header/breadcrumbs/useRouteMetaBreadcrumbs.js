import isNil from 'lodash-es/isNil';
import isString from 'lodash-es/isString';
import isObject from 'lodash-es/isObject';
import isArray from 'lodash-es/isArray';
import has from 'lodash-es/has';

function getRepresentativeLocaleOf(prop, userLocale, fallbacks) {
  if (!prop || isNil(prop) || (!isObject(prop) && !isString(prop))) return undefined;
  // Check if prop is only a string without translations
  if (isString(prop)) return prop;
  // Use language setting of user
  if (has(prop, userLocale)) return userLocale;
  // Iterate over given fallback languages
  if (fallbacks && isArray(fallbacks)) {
    const foundLang = fallbacks.find((lang) => lang
      && isString(lang)
      && has(prop, lang.toLowerCase()));
    if (foundLang) return foundLang;
  }
  // Use the first language in the given property if none of the languages is present
  const key = Object.keys(prop)[0];
  if (key) return key;
  // Use default text if prop does not have any items
  return undefined;
}

function getTranslationFor(prop, userLocale, fallbacks) {
  const locale = getRepresentativeLocaleOf(prop, userLocale, fallbacks);
  return locale
    ? prop[locale]
    : undefined;
}

export default {
  computed: {
    $breadcrumbs() {
      if (!this.$route) {
        console.error('$route not found');
        return [];
      }

      // Go through the route path to build the breadcrumbs
      return this.$route.matched.reduce((acc, route) => {
        const breadcrumbs = route?.meta?.breadcrumbs || null;
        if (breadcrumbs && breadcrumbs.length > 0) {
          const isBreadcrumbFn = breadcrumbs instanceof Function;

          if (isBreadcrumbFn) {
            const langResolverFn = (messages) => getTranslationFor(messages, this.$route.query.locale, ['en']);
            return acc.concat(route?.meta?.breadcrumbs.call(this, {
              route: this.$route, store: this.$store, t: this.$t, langResolverFn,
            }));
          }
          return acc.concat(breadcrumbs);
        }
        return acc;
      }, []);
    },
  },
};
