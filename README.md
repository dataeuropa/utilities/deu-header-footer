# DEU Header Footer

The [Data Europa EU](https://data.europa.eu/en) header and footer for Vue applications.

## Installation

Note that this package is not publicly available and must be installed using the private npm registry https://paca.fokus.fraunhofer.de/#browse/browse:npm-hosted. It is recommended to use a .npmrc file at the root of your project folder. For more information about npm configuration, see [here](https://docs.npmjs.com/cli/v6/configuring-npm/npmrc)

```bash
npm i @deu/deu-header-footer
```

## Releases

Please use semantic relelase keywords to make new releases.

## Usage

Setup the plugin in `main.js` in order to register `deu-header` and `deu-footer` components globally:

```javascript
// In main.js
import deuFooterHeader, { Header, Footer } from '@deu/deu-header-footer';
require('@deu/deu-header-footer/dist/deu-header-footer.css');
// import bootstrap, vue-i18n, and vue-router.
import 'bootstrap';
import 'bootstrap/scss/bootstrap.scss';
import '@ecl/preset-ec/dist/styles/ecl-ec.css';

Vue.use(deuFooterHeader);
Vue.component('deu-header', Header);
Vue.component('deu-footer', Footer);
```

Utilize the now available components:

```html
<template>
    <div>
        <deu-header />
        <!-- Your main content here -->
        <deu-footer />
    </div>
</template>
```

## Demo

This project provides a demo. To use, do

```bash
npm ci
npm run serve
```

which serves the demo page on `localhost` at a free port e.g. `8080`.

## Developing

You can see changes to the source code locally by running the demo. For testing this plugin as an npm package on another project, build and install this plugin like this:
```
npm run build:lib
cd ../{OTHER_PROJECT_FOLDER}
npm install ../{DEU_HEADER_FOOTER_FOLDER}
```

Please refer to [the contributing guide](./CONTRIBUTING.md) before submitting changes to the code.
## Component reference

### `<deu-header>`

**Properties**

| Property                | Type               | Default                                      | Description                                                                                                                           |
|-------------------------|--------------------|----------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| `project` | `String`          | `hub`                                       | Specify project-specific menu items preset. Available projects are `hub`, `metrics`, `statistics`. 
| `active-menu-item`      | `String`, `Number` | `'data'`                                     | Specify the initial collapsed menu item on the primary navigation bar by either referring to their `id` or an index. Available `id` are `data`, `impactAndStudies`, `training`, `newsAndEvents`, `about`.`                  |
| `use-language-selector` | `Boolean`          | `true`                                       | The visibility of the language selector.                                                                                              |
| `menu-items-hook`       | `Function`         | Identity function `(menuItems) => menuItems` | Specify a function that takes an array `menuItems` as parameter in order to return modified or replaced menu items. Use with caution. |

### `<deu-footer>`

**Properties**

| Property                | Type      | Default | Description                               |
|-------------------------|-----------|---------|-------------------------------------------|
| `enable-authentication` | `Boolean` | `false` | If true, displays login/logout buttons.   |
| `authenticated`         | `Boolean` | `false` | Specify the current authentication state. |

**Events**

| Event               | Arguments           | Description                                                                                                    |
|---------------------|---------------------|----------------------------------------------------------------------------------------------------------------|
| `login`             | Native object       | Emitted when clicked on the login button, e.g., to initialize authentication process.                          |
| `logout`            | Native object       | Emitted when clicked on the logout button, e.g., to initialize de-authentication process.                      |
| `click-follow-link` | `String` target URL | Emitted when clicked on one of the social media links, e.g., to track external navigation using web analytics. |


## Dependencies and their licenses

***
This [3rd-party-licenses.csv](3rd-party-licenses.csv) contains a list of the dependencies and their licenses. 
